# Zenodo publication

This repository contains the current [Zenodo publication](https://zenodo.org/record/5091604).
For more information about shepard, its usage and infrastructure, check out [the wiki](https://gitlab.com/dlr-shepard/documentation/-/wikis/home).
